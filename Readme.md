# Installing and Using Sysdig Falco on IBM Cloud

## Using Falco for runtime container security monitoring with IBM Cloud Kubernetes Service

[Falco](https://falco.org/) is a cloud native runtime security system that works with both containers and raw linux hosts. It is developed by [Sysdig](https://sysdig.com/) and is a [sandbox](https://landscape.cncf.io/selected=falco) project in the Cloud Native Computing Foundation. Falco works by looking at file changes, network activity, the process table and more for suspicious behaviour, then sending alerts through a pluggable backend. Falco does this by inspecting at the system call level through a kernel module. Falco contains a rich and user-editable list of rules to flag certain concerning behaviours and whitelist normal computer operations.

In this tutorial, you'll install and setup falco on a kubernetes cluster on the IBM Public Cloud, then use create a synthetic security incident, see the incident in falco. Then finally you'll wire up your falco to send security alerts to slack.

Estimated time: 10 minutes

> Get the source code

```shell
$ git clone https://gitlab.com/nibalizer/falco-iks
$ cd falco-iks
```

> Verify you have an IBM Cloud Kubernetes Cluster and it is set up and configured

```shell
$ ibmcloud ks cluster get nibz-nightly-2019-03-29
Retrieving cluster nibz-nightly-2019-03-29...
OK

                           
Name:                   nibz-nightly-2019-03-29   
ID:                     0bddeb0c936d4b5a831849a399022389   
State:                  normal   
Created:                2019-03-29T08:01:08+0000   
Location:               wdc06   
Master URL:             https://c1.us-east.containers.cloud.ibm.com:30611   
Master Location:        Washington D.C.   
Master Status:          Ready (12 hours ago)   
Ingress Subdomain:      nibz-nightly-2019-03-29.us-east.containers.appdomain.cloud   
Ingress Secret:         nibz-nightly-2019-03-29   
Workers:                3   
Worker Zones:           wdc06   
Version:                1.12.6_1546   
Owner:                  skrum@us.ibm.com   
Monitoring Dashboard:   -   
Resource Group ID:      2a926a9173174d94a6eb13284e089f88   
Resource Group Name:    default   
```

```shell
$ kubectl get nodes -o wide
NAME             STATUS   ROLES    AGE   VERSION       INTERNAL-IP      EXTERNAL-IP      OS-IMAGE             KERNEL-VERSION      CONTAINER-RUNTIME
10.188.103.223   Ready    <none>   12h   v1.12.6+IKS   10.188.103.223   169.63.131.195   Ubuntu 16.04.6 LTS   4.4.0-143-generic   containerd://1.1.6
10.188.103.242   Ready    <none>   12h   v1.12.6+IKS   10.188.103.242   169.63.131.201   Ubuntu 16.04.6 LTS   4.4.0-143-generic   containerd://1.1.6
10.188.103.248   Ready    <none>   12h   v1.12.6+IKS   10.188.103.248   169.63.131.248   Ubuntu 16.04.6 LTS   4.4.0-143-generic   containerd://1.1.6
```


Note the container runtime is `containerd`.



We'll use the `k8s-with-rbac` files since we have Kubernetes 1.12. Falco requires a number of files for set up. This is a combination of Kubernetes configuration to run the falco daemon and configuration for the daemon itself.


> Review Falco installation files

```shell
$ ls -l
total 20
-rw-r--r-- 1 nibz nibz  931 Mar 29 15:46 falco-account.yaml
drwxr-xr-x 2 nibz nibz 4096 Mar 29 15:51 falco-config/
-rw-r--r-- 1 nibz nibz 2138 Mar 29 15:48 falco-daemonset-configmap.yaml
-rw-r--r-- 1 nibz nibz  196 Mar 29 15:45 falco-service.yaml
-rw-r--r-- 1 nibz nibz   13 Mar 29 15:27 Readme.md
```

Falco uses a service account in Kubernetes to access the Kube API. The `falco-account.yaml` spec sets up our common RBAC tripple: a `ServiceAccount`, a `ClusterRole`, and a `ClusterRoleBinding`. The `ClusterRole` has the information around what access is being given. If you changed nothing about these files, the Falco daemon can only read and list, but not modify any object in the Kubernetes API.

> Apply RBAC configuration
```
$ kubectl apply -f falco-account.yaml 
serviceaccount/falco-account created
clusterrole.rbac.authorization.k8s.io/falco-cluster-role created
clusterrolebinding.rbac.authorization.k8s.io/falco-cluster-role-binding created
```

Falco also needs a Kubernetes service for it's web front end.

> Apply the Falco service object.
```shell
$ kubectl apply -f falco-service.yaml 
service/falco-service created
```

Falco's configuration is split up into a number of files. `falco.yaml` refers to configuration of the daemon's particulars: output type, ports, etc. The other `*_rules.yaml` files contain the checks that Falco fires against e.g. shells being opened, files being modified, etc. We lump all of these files into a single configmap by using the `--from-file` argument with a directory. Later, in the deployment of the daemon, we'll mount this config map under `/etc/falco`.

> Create the `falco-config` configmap 
```shell
$ kubectl create configmap falco-config --from-file=falco-config
configmap/falco-config created
```

Finally, we'll run the Falco application. This is being run as a daemonset, which enables us to run one per node. During it's first-run installation, it will use DKMS to compile and install a kernel module, which is how Falco picks up the system calls.


> Start the Falco daemonset
```shell
$ kubectl apply -f falco-daemonset-configmap.yaml
daemonset.extensions/falco-daemonset created
```

> Check that the pods have started correctly

```shell
nibz@shockley:~/projects/falco/install-falco-iks/git-repo$ kubectl get pod
NAME                    READY   STATUS    RESTARTS   AGE
falco-daemonset-99p8j   1/1     Running   0          26s
falco-daemonset-wf2lf   1/1     Running   0          26s
falco-daemonset-wqrwm   1/1     Running   0          26s
```

> Check out the logs, note the messages from DKMS (sysdig is a kernel module!)

```shell
nibz@shockley:~/projects/falco/install-falco-iks/git-repo$ kubectl logs falco-daemonset-wf2lf
* Setting up /usr/src links from host
* Unloading falco-probe, if present
* Running dkms install for falco

Kernel preparation unnecessary for this kernel.  Skipping...

Building module:
cleaning build area...
make -j2 KERNELRELEASE=4.4.0-148-generic -C /lib/modules/4.4.0-148-generic/build M=/var/lib/dkms/falco/0.1.2780dev/build....
cleaning build area...

DKMS: build completed.

falco-probe.ko:
Running module version sanity check.
 - Original module
   - No original module exists within this kernel
 - Installation
   - Installing to /lib/modules/4.4.0-148-generic/kernel/extra/
mkdir: cannot create directory '/lib/modules/4.4.0-148-generic/kernel/extra': Read-only file system
cp: cannot create regular file '/lib/modules/4.4.0-148-generic/kernel/extra/falco-probe.ko': No such file or directory

depmod...

DKMS: install completed.
* Trying to load a dkms falco-probe, if present
falco-probe found and loaded in dkms
Wed May 29 14:55:40 2019: Falco initialized with configuration file /etc/falco/falco.yaml
Wed May 29 14:55:40 2019: Loading rules from file /etc/falco/falco_rules.yaml:
Wed May 29 14:55:40 2019: Loading rules from file /etc/falco/falco_rules.local.yaml:
Wed May 29 14:55:40 2019: Loading rules from file /etc/falco/k8s_audit_rules.yaml:
Wed May 29 14:55:41 2019: Starting internal webserver, listening on port 8765
{"output":"00:00:00.048356736: Informational Container with sensitive mount started (user=root command=container:7c5302fccfcb k8s.ns=<NA> k8s.pod=<NA> container=7c5302fccfcb image=registry.ng.bluemix.net/armada-master/ibm-kube-fluentd-collector:c16fe1602ab65db4af0a6ac008f99ca2a526e6f6 mounts=/etc/kubernetes/:/etc/kubernetes::false:private,/:/host::false:private,/var/log/:/var/log::false:private,/var/lib/docker:/var/lib/docker::false:private,/var/run/docker.sock:/var/run/docker.sock::false:private,/mnt/ibm-kube-fluentd-persist:/mnt/ibm-kube-fluentd-persist::true:private,/var/data/kubelet/pods/8bf0a002-81eb-11e9-b9cf-c68b81a15994/volumes/kubernetes.io~secret/logmet-secrets-volume:/mnt/logmet/secrets::false:private,/var/data/kubelet/pods/8bf0a002-81eb-11e9-b9cf-c68b81a15994/volumes/kubernetes.io~configmap/fluentd-config:/fluentd/etc/config.d/logmet/::false:private,/var/data:/var/data::false:private,/var/data/kubelet/pods/8bf0a002-81eb-11e9-b9cf-c68b81a15994/volumes/kubernetes.io~configmap/at-fluentd-config:/fluentd/etc/config.d/at/::false:private,/var/data/kubelet/pods/8bf0a002-81eb-11e9-b9cf-c68b81a15994/volumes/kubernetes.io~secret/activity-tracker-secrets-volume:/mnt/activity-tracker/secrets/::false:private,/var/log/at:/var/log/at::false:private,/var/log/at-no-rotate:/var/log/at-no-rotate::false:private,/run/containerd:/run/containerd::false:private,/run/containerd/containerd.sock:/run/containerd/containerd.sock::true:private,/var/data/kubelet/pods/8bf0a002-81eb-11e9-b9cf-c68b81a15994/volumes/kubernetes.io~secret/ibm-kube-fluentd-token-v2q5s:/var/run/secrets/kubernetes.io/serviceaccount::false:private,/var/data/kubelet/pods/8bf0a002-81eb-11e9-b9cf-c68b81a15994/etc-hosts:/etc/hosts::true:private,/var/data/kubelet/pods/8bf0a002-81eb-11e9-b9cf-c68b81a15994/containers/fluentd/35f64419:/dev/termination-log::true:private) k8s.ns=<NA> k8s.pod=<NA> container=7c5302fccfcb","priority":"Informational","rule":"Launch Sensitive Mount Container","time":"1970-01-01T00:00:00.048356736Z", "output_fields": {"container.id":"7c5302fccfcb","container.image.repository":"registry.ng.bluemix.net/armada-master/ibm-kube-fluentd-collector","container.image.tag":"c16fe1602ab65db4af0a6ac008f99ca2a526e6f6","container.mounts":"/etc/kubernetes/:/etc/kubernetes::false:private,/:/host::false:private,/var/log/:/var/log::false:private,/var/lib/docker:/var/lib/docker::false:private,/var/run/docker.sock:/var/run/docker.sock::false:private,/mnt/ibm-kube-fluentd-persist:/mnt/ibm-kube-fluentd-persist::true:private,/var/data/kubelet/pods/8bf0a002-81eb-11e9-b9cf-c68b81a15994/volumes/kubernetes.io~secret/logmet-secrets-volume:/mnt/logmet/secrets::false:private,/var/data/kubelet/pods/8bf0a002-81eb-11e9-b9cf-c68b81a15994/volumes/kubernetes.io~configmap/fluentd-config:/fluentd/etc/config.d/logmet/::false:private,/var/data:/var/data::false:private,/var/data/kubelet/pods/8bf0a002-81eb-11e9-b9cf-c68b81a15994/volumes/kubernetes.io~configmap/at-fluentd-config:/fluentd/etc/config.d/at/::false:private,/var/data/kubelet/pods/8bf0a002-81eb-11e9-b9cf-c68b81a15994/volumes/kubernetes.io~secret/activity-tracker-secrets-volume:/mnt/activity-tracker/secrets/::false:private,/var/log/at:/var/log/at::false:private,/var/log/at-no-rotate:/var/log/at-no-rotate::false:private,/run/containerd:/run/containerd::false:private,/run/containerd/containerd.sock:/run/containerd/containerd.sock::true:private,/var/data/kubelet/pods/8bf0a002-81eb-11e9-b9cf-c68b81a15994/volumes/kubernetes.io~secret/ibm-kube-fluentd-token-v2q5s:/var/run/secrets/kubernetes.io/serviceaccount::false:private,/var/data/kubelet/pods/8bf0a002-81eb-11e9-b9cf-c68b81a15994/etc-hosts:/etc/hosts::true:private,/var/data/kubelet/pods/8bf0a002-81eb-11e9-b9cf-c68b81a15994/containers/fluentd/35f64419:/dev/termination-log::true:private","evt.time":48356736,"k8s.ns.name":null,"k8s.pod.name":null,"proc.cmdline":"container:7c5302fccfcb","user.name":"root"}}
{"output":"00:00:00.048356736: Informational Container with sensitive mount started (user=<NA> command=container:721ef5130945 k8s.ns=<NA> k8s.pod=<NA> container=721ef5130945 image=docker.io/falcosecurity/falco:dev mounts=/run/containerd/containerd.sock:/host/run/containerd/containerd.sock::true:private,/dev:/host/dev::true:private,/proc:/host/proc::false:private,/boot:/host/boot::false:private,/lib/modules:/host/lib/modules::false:private,/usr:/host/usr::false:private,/etc:/host/etc/::false:private,/var/data/kubelet/pods/c09f7a8d-8221-11e9-b9cf-c68b81a15994/volumes/kubernetes.io~configmap/falco-config:/etc/falco::false:private,/var/data/kubelet/pods/c09f7a8d-8221-11e9-b9cf-c68b81a15994/volumes/kubernetes.io~secret/falco-account-token-jlddc:/var/run/secrets/kubernetes.io/serviceaccount::false:private,/var/data/kubelet/pods/c09f7a8d-8221-11e9-b9cf-c68b81a15994/etc-hosts:/etc/hosts::true:private,/var/data/kubelet/pods/c09f7a8d-8221-11e9-b9cf-c68b81a15994/containers/falco/aa75ae83:/dev/termination-log::true:private) k8s.ns=<NA> k8s.pod=<NA> container=721ef5130945","priority":"Informational","rule":"Launch Sensitive Mount Container","time":"1970-01-01T00:00:00.048356736Z", "output_fields": {"container.id":"721ef5130945","container.image.repository":"docker.io/falcosecurity/falco","container.image.tag":"dev","container.mounts":"/run/containerd/containerd.sock:/host/run/containerd/containerd.sock::true:private,/dev:/host/dev::true:private,/proc:/host/proc::false:private,/boot:/host/boot::false:private,/lib/modules:/host/lib/modules::false:private,/usr:/host/usr::false:private,/etc:/host/etc/::false:private,/var/data/kubelet/pods/c09f7a8d-8221-11e9-b9cf-c68b81a15994/volumes/kubernetes.io~configmap/falco-config:/etc/falco::false:private,/var/data/kubelet/pods/c09f7a8d-8221-11e9-b9cf-c68b81a15994/volumes/kubernetes.io~secret/falco-account-token-jlddc:/var/run/secrets/kubernetes.io/serviceaccount::false:private,/var/data/kubelet/pods/c09f7a8d-8221-11e9-b9cf-c68b81a15994/etc-hosts:/etc/hosts::true:private,/var/data/kubelet/pods/c09f7a8d-8221-11e9-b9cf-c68b81a15994/containers/falco/aa75ae83:/dev/termination-log::true:private","evt.time":48356736,"k8s.ns.name":null,"k8s.pod.name":null,"proc.cmdline":"container:721ef5130945","user.name":null}}
```

Falco is running and logging events to standard out. Now let's take a peek at the configuration we've set up for it.


> Examine the daemonset configuration:

```shell
$ cat falco-daemonset-configmap.yaml | grep serviceAcc
      serviceAccount: falco-account
```

This is telling the daemonset to run with the service account and permissons we set up earlier. Check out `falco-account.yaml` for details. What it does is provide read access to almost everything in the kubernetes api server.

```shell
$ cat falco-daemonset-configmap.yaml | grep image
          image: falcosecurity/falco:dev
```

We have to use the `dev` tag on the falco docker image because the containerd support is new and is only available in the `dev` channel.


```shell
$ cat falco-daemonset-configmap.yaml | grep -A 11 volumes:
      volumes:
        - name: containerd-socket
          hostPath:
            path: /run/containerd/containerd.sock
        - name: dev-fs
          hostPath:
            path: /dev
        - name: proc-fs
          hostPath:
            path: /proc
        - name: boot-fs
          hostPath:
```

Note that we're mounting in a ton of important directories from the kubernetes host into the falco pod. This enables falco to monitor those directories for changes and other concerning events. Also note we're mapping in the containerd socket in lieu of a docker socket.

> Examine the falco configuration files

```shell
$ ls falco-config/
falco_rules.local.yaml  falco_rules.yaml  falco.yaml  k8s_audit_rules.yaml
```

Falco is configured by several yaml files that we set up via a config map. `falco.yaml` configures server settings and `falco_rules.yaml` contains rules for what to alert on and at what level.

> View a falco rule

```shell
$ cat falco-config/falco_rules.yaml | grep -A 12 'Netcat Remote'
- rule: Netcat Remote Code Execution in Container
  desc: Netcat Program runs inside container that allows remote code execution
  condition: >
    spawned_process and container and
    ((proc.name = "nc" and (proc.args contains "-e" or proc.args contains "-c")) or
     (proc.name = "ncat" and (proc.args contains "--sh-exec" or proc.args contains "--exec"))
    )
  output: >
    Netcat runs inside container that allows remote code execution (user=%user.name
    command=%proc.cmdline container_id=%container.id container_name=%container.name image=%container.image.repository:%container.image.tag)
  priority: WARNING
  tags: [network, process]
```

This rule watches for potentitally nefarious netcat commands and throws alerts when it sees them at the `WARNING` level.


Now lets see falco in action. We'll tail the logs in one terminal, then synthentically create some events in the other terminal and watch the events come through the logs.

> (In the first terminal) Tail the logs

```shell
$ kubectl get pod
NAME                    READY   STATUS    RESTARTS   AGE
falco-daemonset-99p8j   1/1     Running   0          112m
falco-daemonset-wf2lf   1/1     Running   0          112m
falco-daemonset-wqrwm   1/1     Running   0          112m
$ kubectl logs -f falco-daemonset-99p8j
* Setting up /usr/src links from host
* Unloading falco-probe, if present
* Running dkms install for falco

Kernel preparation unnecessary for this kernel.  Skipping...

Building module:
cleaning build area...
make -j2 KERNELRELEASE=4.4.0-148-generic -C /lib/modules/4.4.0-148-generic/build M=/var/lib/dkms/falco/0.1.2780dev/build....
cleaning build area...

DKMS: build completed.

falco-probe.ko:
Running module version sanity check.
 - Original module
   - No original module exists within this kernel
 - Installation
   - Installing to /lib/modules/4.4.0-148-generic/kernel/extra/
mkdir: cannot create directory '/lib/modules/4.4.0-148-generic/kernel/extra': Read-only file system
cp: cannot create regular file '/lib/modules/4.4.0-148-generic/kernel/extra/falco-probe.ko': No such file or directory

depmod...

DKMS: install completed.
* Trying to load a dkms falco-probe, if present
falco-probe found and loaded in dkms
Wed May 29 14:55:36 2019: Falco initialized with configuration file /etc/falco/falco.yaml
Wed May 29 14:55:36 2019: Loading rules from file /etc/falco/falco_rules.yaml:
Wed May 29 14:55:37 2019: Loading rules from file /etc/falco/falco_rules.local.yaml:
Wed May 29 14:55:37 2019: Loading rules from file /etc/falco/k8s_audit_rules.yaml:
Wed May 29 14:55:38 2019: Starting internal webserver, listening on port 8765
{"output":"00:00:00.020155776: Informational Container with sensitive mount started (user=<NA> command=container:9d56002def78 k8s.ns=<NA> k8s.pod=<NA> container=9d56002def78 image=docker.io/falcosecurity/falco:dev mounts=/run/containerd/containerd.sock:/host/run/containerd/containerd.sock::true:private,/dev:/host/dev::true:private,/proc:/host/proc::false:private,/boot:/host/boot::false:private,/lib/modules:/host/lib/modules::false:private,/usr:/host/usr::false:private,/etc:/host/etc/::false:private,/var/data/kubelet/pods/c0a1a131-8221-11e9-b9cf-c68b81a15994/volumes/kubernetes.io~configmap/falco-config:/etc/falco::false:private,/var/data/kubelet/pods/c0a1a131-8221-11e9-b9cf-c68b81a15994/volumes/kubernetes.io~secret/falco-account-token-jlddc:/var/run/secrets/kubernetes.io/serviceaccount::false:private,/var/data/kubelet/pods/c0a1a131-8221-11e9-b9cf-c68b81a15994/etc-hosts:/etc/hosts::true:private,/var/data/kubelet/pods/c0a1a131-8221-11e9-b9cf-c68b81a15994/containers/falco/cb0bed6e:/dev/termination-log::true:private) k8s.ns=<NA> k8s.pod=<NA> container=9d56002def78","priority":"Informational","rule":"Launch Sensitive Mount Container","time":"1970-01-01T00:00:00.020155776Z", "output_fields": {"container.id":"9d56002def78","container.image.repository":"docker.io/falcosecurity/falco","container.image.tag":"dev","container.mounts":"/run/containerd/containerd.sock:/host/run/containerd/containerd.sock::true:private,/dev:/host/dev::true:private,/proc:/host/proc::false:private,/boot:/host/boot::false:private,/lib/modules:/host/lib/modules::false:private,/usr:/host/usr::false:private,/etc:/host/etc/::false:private,/var/data/kubelet/pods/c0a1a131-8221-11e9-b9cf-c68b81a15994/volumes/kubernetes.io~configmap/falco-config:/etc/falco::false:private,/var/data/kubelet/pods/c0a1a131-8221-11e9-b9cf-c68b81a15994/volumes/kubernetes.io~secret/falco-account-token-jlddc:/var/run/secrets/kubernetes.io/serviceaccount::false:private,/var/data/kubelet/pods/c0a1a131-8221-11e9-b9cf-c68b81a15994/etc-hosts:/etc/hosts::true:private,/var/data/kubelet/pods/c0a1a131-8221-11e9-b9cf-c68b81a15994/containers/falco/cb0bed6e:/dev/termination-log::true:private","evt.time":20155776,"k8s.ns.name":null,"k8s.pod.name":null,"proc.cmdline":"container:9d56002def78","user.name":null}}
...
```

> In a second terminal create a security event (remember to re-export `KUBECONFIG`)

```shell
$ export KUBECONFIG=/home/nibz/.bluemix/plugins/container-service/clusters/yourcluster.yml
$ kubectl get pod
NAME                    READY   STATUS    RESTARTS   AGE
falco-daemonset-99p8j   1/1     Running   0          3h2m
falco-daemonset-wf2lf   1/1     Running   0          3h2m
falco-daemonset-wqrwm   1/1     Running   0          3h2m
nibz@shockley:~/projects/falco/install-falco-iks/git-repo$ kubectl  exec -it falco-daemonset-99p8j /bin/bash
root@falco-daemonset-99p8j:/# echo "I'm in!"
I'm in!
root@falco-daemonset-99p8j:/# 
```

In the first terminal you can see the event

```shell
{"output":"17:58:28.064781208: Notice A shell was spawned in a container with an attached terminal (user=root k8s.ns=default k8s.pod=falco-daemonset-99p8j container=9d56002def78 shell=bash parent=<NA> cmdline=bash terminal=34816) k8s.ns=default k8s.pod=falco-daemonset-99p8j container=9d56002def78","priority":"Notice","rule":"Terminal shell in container","time":"2019-05-29T17:58:28.064781208Z", "output_fields": {"container.id":"9d56002def78","evt.time":1559152708064781208,"k8s.ns.name":"default","k8s.pod.name":"falco-daemonset-99p8j","proc.cmdline":"bash","proc.name":"bash","proc.pname":null,"proc.tty":34816,"user.name":"root"}}
```

> Process the event with jq

```shell
$ echo '{"output":"17:58:28.064781208: Notice A shell was spawned in a container with an attached terminal (user=root k8s.ns=default k8s.pod=falco-daemonset-99p8j container=9d56002def78 shell=bash parent=<NA> cmdline=bash terminal=34816) k8s.ns=default k8s.pod=falco-daemonset-99p8j container=9d56002def78","priority":"Notice","rule":"Terminal shell in container","time":"2019-05-29T17:58:28.064781208Z", "output_fields": {"container.id":"9d56002def78","evt.time":1559152708064781208,"k8s.ns.name":"default","k8s.pod.name":"falco-daemonset-99p8j","proc.cmdline":"bash","proc.name":"bash","proc.pname":null,"proc.tty":34816,"user.name":"root"}}
> ' | jq '.'
{
  "output": "17:58:28.064781208: Notice A shell was spawned in a container with an attached terminal (user=root k8s.ns=default k8s.pod=falco-daemonset-99p8j container=9d56002def78 shell=bash parent=<NA> cmdline=bash terminal=34816) k8s.ns=default k8s.pod=falco-daemonset-99p8j container=9d56002def78",
  "priority": "Notice",
  "rule": "Terminal shell in container",
  "time": "2019-05-29T17:58:28.064781208Z",
  "output_fields": {
    "container.id": "9d56002def78",
    "evt.time": 1559152708064781300,
    "k8s.ns.name": "default",
    "k8s.pod.name": "falco-daemonset-99p8j",
    "proc.cmdline": "bash",
    "proc.name": "bash",
    "proc.pname": null,
    "proc.tty": 34816,
    "user.name": "root"
  }
}
```

Notice that falco gives useful information about the security event and the full kubernetes context for the event (pod name, namespace, etc).

Now we can trigger the netcat rule we displayed earlier

```shell
root@falco-daemonset-99p8j:/# nc -l 4444
^C
```

```shell
kubectl logs falco-daemonset-99p8j
...

{"output":"18:00:41.530249297: Notice Network tool launched in container (user=root command=nc -l 4444 container_id=9d56002def78 container_name=falco image=docker.io/falcosecurity/falco:dev) k8s.ns=default k8s.pod=falco-daemonset-99p8j container=9d56002def78 k8s.ns=default k8s.pod=falco-daemonset-99p8j container=9d56002def78","priority":"Notice","rule":"Lauch Suspicious Network Tool in Container","time":"2019-05-29T18:00:41.530249297Z", "output_fields": {"container.id":"9d56002def78","container.image.repository":"docker.io/falcosecurity/falco","container.image.tag":"dev","container.name":"falco","evt.time":1559152841530249297,"k8s.ns.name":"default","k8s.pod.name":"falco-daemonset-99p8j","proc.cmdline":"nc -l 4444","user.name":"root"}}
...
$ echo '{"output":"18:00:41.530249297: Notice Network tool launched in container (user=root command=nc -l 4444 contain    er_id=9d56002def78 container_name=falco image=docker.io/falcosecurity/falco:dev) k8s.ns=default k8s.pod=falco-    daemonset-99p8j container=9d56002def78 k8s.ns=default k8s.pod=falco-daemonset-99p8j container=9d56002def78","priority":"Notice","rule":"Lauch Suspicious Network Tool in Container","time":"2019-05-29T18:00:41.530249297Z",     "output_fields": {"container.id":"9d56002def78","container.image.repository":"docker.io/falcosecurity/falco",    "container.image.tag":"dev","container.name":"falco","evt.time":1559152841530249297,"k8s.ns.name":"default","k    8s.pod.name":"falco-daemonset-99p8j","proc.cmdline":"nc -l 4444","user.name":"root"}}' | jq '.'
{
  "output": "18:00:41.530249297: Notice Network tool launched in container (user=root command=nc -l 4444 contain    er_id=9d56002def78 container_name=falco image=docker.io/falcosecurity/falco:dev) k8s.ns=default k8s.pod=falco-    daemonset-99p8j container=9d56002def78 k8s.ns=default k8s.pod=falco-daemonset-99p8j container=9d56002def78",
  "priority": "Notice",
  "rule": "Lauch Suspicious Network Tool in Container",
  "time": "2019-05-29T18:00:41.530249297Z",
  "output_fields": {
    "container.id": "9d56002def78",
    "container.image.repository": "docker.io/falcosecurity/falco",
    "container.image.tag": "dev",
    "container.name": "falco",
    "evt.time": 1559152841530249200,
    "k8s.ns.name": "default",
    "k8s.pod.name": "falco-daemonset-99p8j",
    "proc.cmdline": "nc -l 4444",
    "user.name": "root"
  }
}

```

You've now seen what kind of events falco can discover and a bit of how to configure them. Now lets do something more interesting with the alert than just dumping it to standard out. Falco has a few different things you can do with these, see https://falco.org/docs/alerts/, but for this tutorial we'll send alerts to a slack room.

> Change `falco-config/falco.yaml` to include your slack webhook and change `enabled` to `true`. See [this](https://api.slack.com/incoming-webhooks) for how to create an incomming webhook in slack.

```shell
$ cat falco-config/falco.yaml | grep -A1 -B3 hooks.slack
--
program_output:
  enabled: false
  keep_alive: false
  program: "jq '{text: .output}' | curl -d @- -X POST https://hooks.slack.com/services/XXX"
```

```shell
vim falco-config/falco.yaml
```

```shell
$ cat falco-config/falco.yaml | grep -A1 -B3 hooks.slack
program_output:
  enabled: true
  keep_alive: false
  program: "jq '{text: .output}' | curl -d @- -X POST https://hooks.slack.com/services/XXXXXXXXX/XXXXXXXXX/xxxxxxxxxxxxxxxxxxxxxxxx"
```

With this, redo both the config map and the daemonset.

```shell

$ kubectl delete configmap falco-config
$ kubectl create configmap falco-config --from-file=falco-config
$ kubectl delete -f falco-daemonset-configmap.yaml
$ kubectl apply -f falco-daemonset-configmap.yaml 
```

With that you can again spawn a shell, and you'll see a security alert posted to slack!


![slack screenshot](.screenshots/slack.png)


There is lots more you can do with falco, including hooking up falco events into serverless computing platforms such as OpenWhisk and Knative. Hopefully this introduction gave you some basic getting started information and will serve as a launching off part to your next project.

Special Thanks: Big thanks to Michael Ducy (@mfdii) for helping get this tutorial working.


Related Links:

https://github.com/falcosecurity/falco/tree/dev/integrations/k8s-using-daemonset
https://falco.org/docs/alerts/
https://api.slack.com/incoming-webhooks
https://landscape.cncf.io/selected=falco


